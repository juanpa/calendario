calendar = {}
def add_activity(date: str, time: str, activity: str):
    if date not in calendar:
        calendar[date] = {}
    calendar[date][time] = activity
def get_time(atime):
    """Devuelve todas las actividades para la hora atime como una lista de tuplas"""
    activities = []
    for date, times in calendar.items():
        for i, activity in times.items():
            if i == atime:
                activities.append((date, i, activity))
    return activities
def get_all():
    """Devuelve todas las actividade sen el calendario como una lista de tuplas"""
    all = []
    for date, times in calendar.items():
        for t, activity in times.items():
            all.append((date, t, activity))
    return all
def get_busiest():
    """Devuelve la fecha con más actividades, y su número de actividades"""
    busiest = ""
    busiest_no = 0
    for date, times in calendar.items():
        if len(times) > busiest_no:
            busiest = date
            busiest_no = len(times)
    return (busiest, busiest_no)
def show(activities):
    for (date, time, activity) in activities:
        print(f"{date}. {time}: {activity}")
def check_date(date):
    """Comprueba si una fecha tiene el formato correcto (aaaa-mm-dd)
    Devuelve True o False"""
    try:
        year, month, day = map(int, date.split('-'))
        if 0000 <= year <= 2100 and 1 <= month <= 12 and 1 <= day <= 31:
            return True
    except ValueError:
        pass
    return False
def check_time(time):
    """Comprueba si una hora tiene el formato correcto (hh:mm)
    Devuelve True o False"""
    try:
        hour, minute = map(int, time.split(':'))
        if 0 <= hour <= 23 and 0 <= minute <= 59:
            return True
    except ValueError:
        pass
    return False
def get_activity():
    """"Pide al usuario una actividad
    En caso de error al introducirla, vuelve a pedirla desde el principio"""
    while True:
        date = input("Fecha: ")
        if not check_date(date):
            print("Formato de fecha incorrecto. Debe ser aaaa-mm-dd.")
            continue

        time = input("Hora: ")
        if not check_time(time):
            print("Formato de hora incorrecto. Debe ser hh:mm.")
            continue
        activity = input("Actividad: ")
    return (date, time, activity)
def menu():
    print("A. Introduce actividad")
    print("B. Lista todas las actividades")
    print("C. Día más ocupado")
    print("D. Lista de las actividades de cierta hora dada")
    print("X. Terminar")
    asking = True
    while asking:
        option = input("Opción: ").upper()
        if option in ['A', 'B', 'C', 'D', 'X']:
            asking = False
    return option
def run_option(option):
    """Ejecuta el código necesario para la opción dada"""
    if option == 'A':
        date, time, activity = get_activity()
        add_activity(date, time, activity)
    elif option == 'B':
        activities = get_all()
        show(activities)
    elif option == 'C':
        busiest, busiest_no = get_busiest()
        print(f"El día más ocupado es el {busiest}, con {busiest_no} actividad(es).")
    elif option == 'D':
        atime = input("Hora: ")
        activities = get_time(atime)
        show(activities)
def main():
    proceed = True
    while proceed:
        option = menu()
        if option == 'X':
            proceed = False
        else:
            run_option(option)

if __name__ == "__main__":
    main()
